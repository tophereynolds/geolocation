# Run these commands before executing any build jobs,
# such as to install dependencies and set environment variables
#
before_script:
    # Decrypt server key
    - openssl enc -aes-256-cbc -md sha256 -salt -d -in assets/server.key.enc -out assets/server.key -k $SERVER_KEY_PASSWORD -pbkdf2
    - openssl enc -aes-256-cbc -md sha256 -salt -d -in assets/sandbox/server.key.enc -out assets/sandbox/server.key -k $SERVER_KEY_PASSWORD -pbkdf2
    # Install jq, a json parsing library
    - apt update && apt -y install jq 
    # Setup SFDX environment variables 
    # https://developer.salesforce.com/docs/atlas.en-us.sfdx_dev.meta/sfdx_dev/sfdx_dev_cli_env_variables.htm
    - export SALESFORCE_CLI_URL=https://developer.salesforce.com/media/salesforce-cli/sfdx-linux-amd64.tar.xz
    - export SFDX_AUTOUPDATE_DISABLE=false
    - export SFDX_USE_GENERIC_UNIX_KEYCHAIN=true
    - export SFDX_DOMAIN_RETRY=600
    - export SFDX_LOG_LEVEL=DEBUG
    # Install Salesforce CLI
    - mkdir sfdx
    - wget -qO- $SALESFORCE_CLI_URL | tar xJ -C sfdx --strip-components 1
    - './sfdx/install'
    - export PATH=./sfdx/$(pwd):$PATH
    # Output CLI version and plug-in information
    - sfdx update
    - sfdx --version
    - sfdx plugins --core
#
# Define the stages of our pipeline
#
stages:
    - Scratch-Org-Code-Test
    - Sandbox-Integration-Test
    - Push-to-Model-Functional-Test
    - Deploy-to-Prodcution
#
# Stage 1 -- Create a scratch org for code testing
#
Scratch-Org-Code-Test:
    stage: Scratch-Org-Code-Test
    script:
        # Authenticate to the Dev Hub using the server key
        - sfdx force:auth:jwt:grant --setdefaultdevhubusername --clientid $SF_CONSUMER_KEY --jwtkeyfile assets/server.key --username $SF_USERNAME
        # Create scratch org
        - sfdx force:org:create --setdefaultusername --definitionfile config/project-scratch-def.json --wait 10 --durationdays 1
        - sfdx force:org:display
        # Push source to scratch org (this is with source code, all files, etc)
        - sfdx force:source:push
        # Add sample data into app (into the org...)
        - sfdx force:data:tree:import -f data/Account.json
        # Assign as permission set
        - sfdx force:user:permset:assign -n Geolocation 
        # Unit Testing
        - sfdx force:apex:test:run --wait 10 --resultformat human --codecoverage --testlevel RunLocalTests
        # Get the username for the scratch org
        - export SCRATCH_ORG_USERNAME="$(eval sfdx force:user:display --json | jq -r '.result.username')"
        - echo "$SCRATCH_ORG_USERNAME" > ./SCRATCH_ORG_USERNAME.TXT
        # Generate a new password for the scrach org 
        - sfdx force:user:password:generate
        # Display username, password, and instance URL for login
        # Be careful not to do this in a publicly accessible pipeline as it exposes the credentials of your scratch org
        - sfdx force:user:display
        # Delete Scratch Org
        # - sfdx force:org:delete --noprompt
#
# Stage 2 -- Create a scratch org, create a package version, and push into org for testing
#
Sandbox-Integration-Test:
    # Specify file paths that we want available
    # in downstream build stages for this pipeline execution.
    # This is a way to pass dynamic values from one stage to another.
    # artifacts:
    #     paths:
    #         - PACKAGE_VERSION_ID.TXT
    #         - SCRATCH_ORG_USERNAME.TXT
    stage: Sandbox-Integration-Test
    script:
        # Authenticate to the Dev Hub using the server key
        # - sfdx force:auth:jwt:grant --setdefaultdevhubusername --clientid $SF_CONSUMER_KEY --jwtkeyfile assets/sandbox/server.key --username $SF_USERNAME  
        - sfdx force:auth:jwt:grant --setdefaultdevhubusername --clientid $SF_SANDBOX_CONSUMER_KEY --jwtkeyfile assets/sandbox/server.key --username $SF_SANDBOX_USER --instanceurl https://test.salesforce.com --setalias TRFeature
        # Create scratch org
        # - sfdx force:org:create --setdefaultusername --definitionfile config/project-scratch-def.json --wait 10 --durationdays 1 -a HPCTest2
        - sfdx force:org:display -u TRFeature
        - sfdx force:source:push -f -u TRFeature
        # - sfdx force:source:deploy -u HPCTest2 --wait 35 --testlevel RunLocalTests -x ./mdapipkg/package.xml
        # Add sample data into app (into the org...)
        # - sfdx force:data:tree:import -f data/Building__c.json -u TRFeature
        # Assign as permission set (throws error is already assigned in sndbox)
        # - sfdx force:user:permset:assign -n Geolocation -u TRFeature
        # Run all the tests on the sandbox
        - sfdx force:apex:test:run --wait 10 --resultformat human --codecoverage --testlevel RunLocalTests -u TRFeature 
#
# Stage 3 -- Promote the package to downstream environment for functional tests in Model
#
Push-to-Model-Functional-Test:
    stage: Push-to-Model-Functional-Test
    # This stage must be started manually as an example of
    # conditional stages that need to wait for an approval,
    # such as waiting for QA signoff from the previous stage.
    when: manual
    script:
        # Authenticate to the Dev Hub using the server key
        # - sfdx force:auth:jwt:grant --setdefaultdevhubusername --clientid $SF_CONSUMER_KEY --jwtkeyfile assets/sandbox/server.key --username $SF_USERNAME  
        - sfdx force:auth:jwt:grant --setdefaultdevhubusername --clientid $SF_SANDBOX_CONSUMER_KEY --jwtkeyfile assets/sandbox/server.key --username $SF_SANDBOX_USER --instanceurl https://test.salesforce.com --setalias Model
        # Create scratch org
        # - sfdx force:org:create --setdefaultusername --definitionfile config/project-scratch-def.json --wait 10 --durationdays 1 -a HPCTest2
        - sfdx force:org:display -u Model
        - sfdx force:source:push -f -u Model
        # - sfdx force:source:deploy -u HPCTest2 --wait 35 --testlevel RunLocalTests -x ./mdapipkg/package.xml
        # Add sample data into app (into the org...)
        # - sfdx force:data:tree:import -f data/Building__c.json -u Model
        # Assign as permission set (throws error is already assigned in sndbox)
        # - sfdx force:user:permset:assign -n Geolocation -u Model
        # Run all the tests on the sandbox
        - sfdx force:apex:test:run --wait 10 --resultformat human --codecoverage --testlevel RunLocalTests -u Model 
#
# Stage 4 -- Promote the package to downstream environment for UAT for example
#
Deploy-to-Prodcution:
    stage: Deploy-to-Prodcution
    # This stage must be started manually as an example of
    # conditional stages that need to wait for an approval,
    # such as waiting for QA signoff from the previous stage.
    when: manual
    script:
        # Authenticate with your playground or sandbox environment
        - sfdx force:auth:jwt:grant --setdefaultdevhubusername --clientid $SF_CONSUMER_KEY --jwtkeyfile assets/server.key --username $SF_USERNAME
        - sfdx force:mdapi:deploy -d mdapioutput --wait 20